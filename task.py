import math
from typing import Union
   
NumType = Union[int, float]


# version1

def some_expression_with_rounding(a: NumType, b: NumType) -> NumType:
    math_expression = (12 * a + 25 * b) / (1 + a**(2**b))	
    result = round(math_expression, 2)	
    # add your code here
    # print(result)
    return result


if __name__ == "__main__":
    print(some_expression_with_rounding(2.5, 1.9))




   

 


import unittest
from task import some_expression_with_rounding

class TestSomeExpressionWithRounding(unittest.TestCase):

    def test_case1(self):
        result = some_expression_with_rounding(2.5, 1.9)
        self.assertEqual(result, 2.46)

    def test_case2(self):
        result = some_expression_with_rounding(2.5, 1.9)
        self.assertEqual(result, 2.460)
   

    # Add more test cases here

if __name__ == '__main__':
    unittest.main()

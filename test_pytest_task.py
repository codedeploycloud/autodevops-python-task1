import pytest
import sys
from task import some_expression_with_rounding
#    .    Windows
#         Linux 

print(sys.path)

# @pytest.fixture
def test_func():
    #result = some_expression_with_rounding(2.5, 1.9) + x
    #return result + 1
    assert some_expression_with_rounding(2.5, 1.9) == 2.46
    assert some_expression_with_rounding(2, 6) == 0.0
    assert some_expression_with_rounding(1.3, -79.7) == -988.45
    
    
 


# content of pytest_task.py
def func(x):
    return x + 1

def test_answer():
    assert func(3) == 4
